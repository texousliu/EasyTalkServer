package cn.texous.easytalk.commonutil.model.redis;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:03
 */
@Data
public class RedisUser {

    private Long id;

    private String code;

    /**
     * 用户名
     */
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 签名
     */
    private String sign;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 备注
     */
    private String remarks;

}
