package cn.texous.easytalk.commonutil.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/3 19:47
 */
public interface UserConst {

    /**
     * 状态枚举类。
     */
    @Getter
    @AllArgsConstructor
    enum Status {
        WAIT("待激活"),
        NORMAL("正常"),
        FREEZE("冻结"),
        LOGOUT("注销"),
        DELETE("已删除"),
        ;

        private String desc;
    }

}
