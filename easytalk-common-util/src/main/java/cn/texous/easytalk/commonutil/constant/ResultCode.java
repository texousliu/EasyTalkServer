package cn.texous.easytalk.commonutil.constant;

import lombok.Getter;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/7/13 19:45
 */
@Getter
public enum ResultCode implements Code {
    /** 成功 */
    SUCCESS(200, "成功"),
    /** 失败 */
    FAIL(400, "失败"),

    /** 未认证（签名错误） */
    UNAUTHORIZED(401, "未认证"),
    /** 权限不足 */
    INSUFFICIENT_PERMISSIONS(403, "权限不足"),
    /** 接口不存在 */
    NOT_FOUND(404, "接口不存在"),

    /** 参数格式不正确 */
    PARAM_FAIL(455, "参数格式不正确"),

    /** 服务器内部错误 */
    INTERNAL_SERVER_ERROR(500, "服务器内部错误"),

    /** 用户已存在 */
    USER_EXISTS_ERROR(10000, "用户已存在"),
    /** 用户不存在 */
    USER_NOT_EXISTS_ERROR(10001, "用户不存在"),
    /** 用户未登录 */
    USER_UNLOGIN_ERROR(10002, "用户未登录"),
    /** 用户名或密码错误 */
    USERNAME_PASSWORD_ERROR(10003, "用户名或密码错误"),

    /** Redis 消息错误 */
    REDIS_MESSAGE_EMPTY_ERROR(20001, "Redis 消息错误"),
    ;

    private int code;
    private String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

}
