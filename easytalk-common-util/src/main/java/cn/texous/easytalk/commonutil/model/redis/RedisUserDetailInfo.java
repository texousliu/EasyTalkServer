package cn.texous.easytalk.commonutil.model.redis;

import lombok.Data;

import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:01
 */
@Data
public class RedisUserDetailInfo {

    private RedisUser user;
    private List<RedisChatGroup> groups;

}
