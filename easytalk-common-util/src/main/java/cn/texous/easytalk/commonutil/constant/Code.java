package cn.texous.easytalk.commonutil.constant;

/**
 * insert description here.
 *
 * @author leven
 * @since 2019/7/13 19:45
 */
public interface Code {

    int getCode();

    String getMessage();

}
