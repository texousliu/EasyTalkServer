package cn.texous.easytalk.commonutil.model.redis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/30 12:30
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RedisMsgDetail {
    // 消息发起人
    private String fromCode;
    // 消息接收人
    private List<String> toCodes;
    // 消息发起人姓名
    private String fromName;

}
