package cn.texous.easytalk.commonutil.constant;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * insert description here
 *
 * @param <T> 数据类型
 * @author leven
 * @since 2019/7/13 19:45
 */
@Getter
@Setter
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -7196540422674459965L;

    private int code;
    private String message;
    private T data;

    public Result() {
    }

    public Result(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(Code resultCode, T data) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
        this.data = data;
    }

    public static Result ok() {
        return out(ResultCode.SUCCESS);
    }

    public static Result ok(Object data) {
        return out(ResultCode.SUCCESS, data);
    }

    public static Result fail(String message) {
        return out(ResultCode.FAIL.getCode(), message, null);
    }

    public static Result fail(String message, Object data) {
        return out(ResultCode.FAIL.getCode(), message, data);
    }

    public static Result paramFail(String message) {
        return out(ResultCode.PARAM_FAIL.getCode(), message, null);
    }

    public static Result paramFail(String message, Object data) {
        return out(ResultCode.PARAM_FAIL.getCode(), message, data);
    }

    public static Result out(Code resultCode) {
        return out(resultCode, null);
    }

    public static Result out(Code resultCode, Object data) {
        return new Result(resultCode, data);
    }

    public static Result out(int code, String message, Object data) {
        return new Result(code, message, data);
    }

}
