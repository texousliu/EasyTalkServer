package cn.texous.easytalk.commonutil.model.redis;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:03
 */
@Data
public class RedisChatGroup {

    private Long chatGroupId;

    private String chatGroupCode;

    /**
     * 群名称
     */
    private String chatGroupName;

    /**
     * 群头像
     */
    private String chatGroupAvatar;

    /**
     * 群主
     */
    private String chatGroupMaster;

    /**
     * 说明
     */
    private String chatGroupRemarks;

    /**
     * et_user 表 code
     */
    private String userCode;

    /**
     * 用户群昵称
     */
    private String nickname;

    /**
     * 状态：0=待生效，1=正常，4=删除
     */
    private Integer status;

}
