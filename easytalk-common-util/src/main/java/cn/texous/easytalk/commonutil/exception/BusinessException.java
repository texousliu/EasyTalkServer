package cn.texous.easytalk.commonutil.exception;


import cn.texous.easytalk.commonutil.constant.Code;
import lombok.Getter;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/4/28 10:26
 */
@Getter
public class BusinessException extends RuntimeException {

    private int code;
    private String message;

    public BusinessException(Code resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public BusinessException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BusinessException(int code, String message, Throwable cause) {
        super(cause);
        this.code = code;
        this.message = message;
    }

    public BusinessException(Code resultCode, Throwable cause) {
        super(cause);
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

}
