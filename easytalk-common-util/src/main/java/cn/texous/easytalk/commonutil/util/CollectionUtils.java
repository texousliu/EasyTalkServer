package cn.texous.easytalk.commonutil.util;

import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/5/15 13:55
 */
public class CollectionUtils {

    /**
     * 从两个集合中移除相同的部分
     *
     * @param t1 第一个集合
     * @param t2 第二个集合
     * @param <T> 集合存储的数据类型
     */
    public static <T> void removeSame(Collection<T> t1, Collection<T> t2) {
        List<T> temp = Lists.newArrayList(t1);
        t1.removeAll(t2);
        t2.removeAll(temp);
    }

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }



}
