package cn.texous.easytalk.commonutil.model.redis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/30 12:30
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RedisMsg {

    private String type;
    private RedisMsgDetail detail;

}
