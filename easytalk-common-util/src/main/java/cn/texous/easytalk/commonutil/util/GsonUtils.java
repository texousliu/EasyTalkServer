package cn.texous.easytalk.commonutil.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;

/**
 * Gson 工具类
 *
 * @author leven
 * @since 2019-08-08 12：12：12
 */
@Slf4j
public class GsonUtils {
    /** Gson 对象 */
    public static final Gson GSON = new Gson();
    /** 美化输出的 Gson 对象 */
    public static final Gson PGSON = (new GsonBuilder()).setPrettyPrinting().create();

    public GsonUtils() {
    }

    public static String toJson(Object o) {
        return GSON.toJson(o);
    }

    public static <T> T fromJson(String source, Class<T> t) {
        return GSON.fromJson(source, t);
    }

    public static <T> T fromJson(String source, Type type) {
        return GSON.fromJson(source, type);
    }

    public static String toPrettyJson(Object o) {
        return PGSON.toJson(o);
    }

    public static void infoPrint(Object o) {
        log.info(GSON.toJson(o));
    }

    public static void prettyInfoPrint(Object o) {
        log.info(PGSON.toJson(o));
    }

    public static void debugPrint(Object o) {
        log.debug(GSON.toJson(o));
    }

    public static void prettyDebugPrint(Object o) {
        log.debug(PGSON.toJson(o));
    }
}
