package cn.texous.easytalk.commonutil.constant;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/3 20:18
 */
public interface RedisKey {

    /** 登陆用户信息存储 key : 替换 token */
    String USER_LOGIN_TOKEN = "user_login_token:%s";
    /** 存储用户 code 和 token 映射，用户只能在一个地方登陆 : 替换 code */
    String USER_LOGIN_CODE_TOKEN = "user_login_code_token:%s";
    /** 存储用户详细信息 : 替换 code */
    String USER_DETAIL_INFO = "user_detail_info:%s";


    /** 用户退出登录消息 */
    String CHANNEL_USER_LOGOUT_MSG = "channel_user_logout_msg";
    /** manager 系统 redis 发布订阅消息 */
    String CHANNEL_MANAGER_SYSTEM_EVENT_MSG = "channel_manager_system_event_msg";

}
