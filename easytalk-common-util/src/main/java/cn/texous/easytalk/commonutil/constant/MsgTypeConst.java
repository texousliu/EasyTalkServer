package cn.texous.easytalk.commonutil.constant;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019/9/4 18:36
 */
public interface MsgTypeConst {

    /** 心跳包 */
    String HEARTBEAT = "0000";
    /** 连接就绪包 */
    String CONNECTION = "0001";
    /** 聊天包 */
    String CHAT = "0002"; 


    /** 好友申请 */
    String ADD_FRIEND_REQ = "1000"; 
    /** 同意好友申请 */
    String ADD_FRIEND_COMFIRM = "1001"; 
    /** 入群申请 */
    String ADD_GROUP_REQ = "1002"; 
    /** 同意入群申请 */
    String ADD_GROUP_CONFIRM = "1003"; 
    /** 邀请入群 */
    String INVITE_ADD_GROUP_REQ = "1004"; 
    /** 同意邀请入群 */
    String INVITE_ADD_GROUP_COMFIRM = "1005"; 
    /** 删除好友 */
    String DELETE_FRIENDS_REQ = "1006"; 
    
}
