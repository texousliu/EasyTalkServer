package cn.texous.easytalk.websocket.common.constants;

import cn.texous.easytalk.commonutil.constant.Code;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/28 15:02
 */
@Getter
@AllArgsConstructor
public enum WsCode implements Code {

    /** 不支持的消息类型 */
    MESSAGE_TYPE_UNSUPPORT(10001, "不支持的消息类型"),
    /** 找不到对应的消息处理类 */
    MESSAGE_HANDER_UNSUPPORT(10002, "找不到对应的消息处理类"),
    ;
    private int code;
    private String message;
}
