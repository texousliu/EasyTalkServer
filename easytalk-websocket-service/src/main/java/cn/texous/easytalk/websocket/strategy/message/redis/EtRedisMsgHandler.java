package cn.texous.easytalk.websocket.strategy.message.redis;

import cn.texous.easytalk.commonutil.model.redis.RedisMsg;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/30 12:03
 */
public interface EtRedisMsgHandler {

    Object hander(RedisMsg message) throws Exception;

}
