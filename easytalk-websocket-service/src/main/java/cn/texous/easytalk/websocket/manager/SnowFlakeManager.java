package cn.texous.easytalk.websocket.manager;

import cn.texous.easytalk.commonutil.util.SnowFlake;
import cn.texous.easytalk.websocket.config.ServerConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 雪花算法管理器
 *
 * @author leven
 * @since
 */
@Slf4j
@Component
public class SnowFlakeManager {

    @Autowired
    private ServerConfig serverConfig;
    private SnowFlake snowFlake;

    @PostConstruct
    public void init() {
        int serverId = serverConfig.getClusterId();
        snowFlake = new SnowFlake(serverId, serverId);
        log.info("this serverId is: {}", serverId);
    }

    /**
     * 生成唯一ID
     *
     * @return
     */
    public long generateId() {
        return snowFlake.nextId();
    }

}
