package cn.texous.easytalk.websocket.strategy.message.chat;

import cn.texous.easytalk.websocket.config.ServerConfig;
import cn.texous.easytalk.websocket.model.entity.ChatMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.tio.core.ChannelContext;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/28 14:23
 */
@Slf4j
public abstract class AbstractEtMessageHandler implements EtMessageHandler {

    @Autowired
    protected ServerConfig serverConfig;

    /**
     * 保存信息
     *
     * @param chatMsg    信息
     * @param readStatus 是否已读
     */
    protected void saveMessage(ChatMsg chatMsg, String readStatus) {
        // TODO 调用微服务 保存消息， 可使用异步操作
    }

    /**
     * 未读消息
     *
     * @param channelContext channelContext
     */
    protected void sendOffLineMessage(ChannelContext channelContext) {
        // TODO 调用微服务获取消息，然后发送消息
        //  List<ImMessage> imMessageList = iImMessageService
        //          .getUnReadMessage(channelContext.userid);
        //  for (ImMessage imMessage : imMessageList) {
        //      Message message = new Message();
        //      message.setId(imMessage.getToId());
        //      message.setMine(false);
        //      message.setType(imMessage.getType());
        //      ImUser imUser = imUserService.getById(imMessage.getFromId());
        //      message.setUsername(imUser.getName());
        //      message.setCid(String.valueOf(imMessage.getId()));
        //      message.setContent(imMessage.getContent());
        //      message.setTimestamp(System.currentTimeMillis());
        //      message.setFromid(imMessage.getFromId());
        //      message.setAvatar(imUser.getAvatar());
        //      SendInfo sendInfo1 = new SendInfo();
        //      sendInfo1.setCode(ChatUtils.MSG_MESSAGE);
        //      sendInfo1.setMessage(message);
        //      WsResponse wsResponse = WsResponse
        //              .fromText(obj2String(sendInfo1), ImTioServerConfig.CHARSET);
        //      Tio.sendToUser(channelContext.groupContext, message.getId(), wsResponse);
        //  }
    }
}
