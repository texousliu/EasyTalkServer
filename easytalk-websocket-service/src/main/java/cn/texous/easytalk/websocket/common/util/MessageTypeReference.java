package cn.texous.easytalk.websocket.common.util;

import cn.texous.easytalk.websocket.model.entity.Message;
import com.fasterxml.jackson.core.type.TypeReference;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * insert description here
 *
 * @param <T> type
 * @author Showa.L
 * @since 2019/8/30 14:46
 */
public class MessageTypeReference<T> extends TypeReference<Message<T>> {

    private Class<T> tClass;

    public MessageTypeReference(Class<T> tClass) {
        this.tClass = tClass;
    }

    @Override
    public Type getType() {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[]{tClass};
            }

            @Override
            public Type getRawType() {
                return Message.class;
            }

            @Override
            public Type getOwnerType() {
                return null;
            }
        };
    }
}
