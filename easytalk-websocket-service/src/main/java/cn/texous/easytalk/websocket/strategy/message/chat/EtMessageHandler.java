package cn.texous.easytalk.websocket.strategy.message.chat;

import org.tio.core.ChannelContext;

/**
 * 消息处理 接口
 *
 * @author Showa.L
 * @since 2019/8/28 14:18
 */
public interface EtMessageHandler {

    Object handler(String msgText, ChannelContext channelContext) throws Exception;

}
