package cn.texous.easytalk.websocket.context;

import cn.texous.easytalk.websocket.common.constants.RedisMsgEnum;
import cn.texous.easytalk.websocket.strategy.message.redis.EtRedisMsgHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/28 14:47
 */
@Component
public class EtRedisMsgHandlerContext {

    private static final Map<String, EtRedisMsgHandler> ET_REDIS_MSG_HANDLER_MAP = new HashMap<>();

    @Autowired
    private void init(Map<String, EtRedisMsgHandler> redisMsgHandlerMap) {
        redisMsgHandlerMap.forEach((k, v) ->
                ET_REDIS_MSG_HANDLER_MAP.put(RedisMsgEnum.MsgType.loadByHandler(k), v)
        );
    }

    public EtRedisMsgHandler loadByType(String type) {
        return ET_REDIS_MSG_HANDLER_MAP.get(type);
    }

}
