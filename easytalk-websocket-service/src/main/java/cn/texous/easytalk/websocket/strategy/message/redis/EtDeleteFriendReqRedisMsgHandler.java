package cn.texous.easytalk.websocket.strategy.message.redis;

import cn.texous.easytalk.commonutil.model.redis.RedisMsg;
import cn.texous.easytalk.websocket.common.util.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tio.core.Tio;
import org.tio.websocket.starter.TioWebSocketServerBootstrap;

import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/10 19:42
 */
@Slf4j
@Component(EtDeleteFriendReqRedisMsgHandler.BEAN_NAME)
public class EtDeleteFriendReqRedisMsgHandler extends AbstractEtRedisMsgHander {

    /** bean name */
    public static final String BEAN_NAME = "etDeleteFriendReqRedisMsgHandler";

    @Autowired
    private TioWebSocketServerBootstrap bootstrap;

    @Override
    public Object hander(RedisMsg message) throws Exception {
        log.info("delete friend request redis msg handler");
        List<String> codes = message.getDetail().getToCodes();
        message.getDetail().setToCodes(null);
        String msgStr = MessageUtils.messageToText(message);
        codes.forEach(code ->
            Tio.sendToUser(bootstrap.getServerGroupContext(), code, getResponse(msgStr))
        );
        return null;
    }
}
