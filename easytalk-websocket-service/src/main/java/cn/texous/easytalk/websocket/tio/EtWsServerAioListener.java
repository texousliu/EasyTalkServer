package cn.texous.easytalk.websocket.tio;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.tio.core.ChannelContext;
import org.tio.core.intf.Packet;
import org.tio.websocket.server.WsServerAioListener;

/**
 * TIO 连接监控类
 *
 * @author Showa.L
 * @since 2019/8/28 14:23
 */
@Slf4j
@Component
public class EtWsServerAioListener extends WsServerAioListener {

    @Override
    public void onAfterConnected(ChannelContext channelContext,
                                 boolean isConnected,
                                 boolean isReconnect) throws Exception {
        super.onAfterConnected(channelContext, isConnected, isReconnect);
    }

    @Override
    public void onAfterSent(ChannelContext channelContext,
                            Packet packet,
                            boolean isSentSuccess) throws Exception {
        super.onAfterSent(channelContext, packet, isSentSuccess);
    }

    @Override
    public void onBeforeClose(ChannelContext channelContext,
                              Throwable throwable,
                              String remark,
                              boolean isRemove) throws Exception {
        super.onBeforeClose(channelContext, throwable, remark, isRemove);
    }

    @Override
    public void onAfterDecoded(ChannelContext channelContext,
                               Packet packet,
                               int packetSize) throws Exception {
        super.onAfterDecoded(channelContext, packet, packetSize);
    }

    @Override
    public void onAfterReceivedBytes(ChannelContext channelContext,
                                     int receivedBytes) throws Exception {
        super.onAfterReceivedBytes(channelContext, receivedBytes);
    }

    @Override
    public void onAfterHandled(ChannelContext channelContext,
                               Packet packet,
                               long cost) throws Exception {
        super.onAfterHandled(channelContext, packet, cost);
    }
}
