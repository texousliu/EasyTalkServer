package cn.texous.easytalk.websocket.model.entity;

import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/30 13:51
 */
@Data
public class MsgType {

    private String type;

}
