package cn.texous.easytalk.websocket.strategy.message.chat;

import org.springframework.stereotype.Component;
import org.tio.core.ChannelContext;
import org.tio.core.Tio;
import org.tio.websocket.common.WsResponse;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/28 14:24
 */
@Component(EtHeartbeatMessageHandler.BEAN_NAME)
public class EtHeartbeatMessageHandler extends AbstractEtMessageHandler {

    /** bean name */
    public static final String BEAN_NAME = "etHeartbeatMessageHandler";

    @Override
    public Object handler(String msgText, ChannelContext channelContext) {
        WsResponse wsResponse = WsResponse.fromText(msgText, serverConfig.getTioMsgCharset());
        Tio.send(channelContext, wsResponse);
        return null;
    }

}
