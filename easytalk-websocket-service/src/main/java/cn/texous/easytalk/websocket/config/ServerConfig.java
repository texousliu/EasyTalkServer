package cn.texous.easytalk.websocket.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/29 11:32
 */
@Data
@Component
@ConfigurationProperties(prefix = "texous.websocket.config")
public class ServerConfig {

    private int clusterId;
    private String tioMsgCharset;

}
