package cn.texous.easytalk.websocket.foreign;

import cn.texous.easytalk.manager.api.feign.EasytalkManagerFeignApiClient;
import cn.texous.easytalk.manager.api.feign.fallback.EasytalkManagerFeignApiClientFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/30 17:57
 */
@FeignClient(name = EasytalkManagerFeignApiClient.NAME,
        fallbackFactory = EasytalkManagerFeignApiClientFallbackFactory.class)
public interface EasytalkManagerFeignClient extends EasytalkManagerFeignApiClient {
}
