package cn.texous.easytalk.websocket.common.constants;

import cn.texous.easytalk.commonutil.constant.MsgTypeConst;
import cn.texous.easytalk.commonutil.exception.BusinessException;
import cn.texous.easytalk.websocket.strategy.message.chat.EtChatMessageHandler;
import cn.texous.easytalk.websocket.strategy.message.chat.EtConnectMessageHandler;
import cn.texous.easytalk.websocket.strategy.message.chat.EtHeartbeatMessageHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/29 13:59
 */
public interface ChatEnum {

    /** 消息类型 */
    @Getter
    @AllArgsConstructor
    enum MsgType {
        HEARTBEAT(MsgTypeConst.HEARTBEAT, EtHeartbeatMessageHandler.BEAN_NAME, "心跳包"),
        CONNECTION(MsgTypeConst.CONNECTION, EtConnectMessageHandler.BEAN_NAME, "连接就绪包"),
        CHAT(MsgTypeConst.CHAT, EtChatMessageHandler.BEAN_NAME, "聊天包"),
        ;

        private String type;
        private String handler;
        private String desc;

        public static String loadByHandler(String handler) {
            return Stream.of(MsgType.values())
                    .filter(p -> p.getHandler().equals(handler))
                    .findFirst()
                    .map(MsgType::getType)
                    .orElseThrow(() -> new BusinessException(WsCode.MESSAGE_HANDER_UNSUPPORT));
        }
    }

    /** 消息状态 */
    @Getter
    @AllArgsConstructor
    enum MsgStatus {
        UNREAD("0", "未读"),
        HAVE_READ("1", "已读"),
        ;

        private String status;
        private String desc;
    }

    /** 聊天类型 */
    @Getter
    @AllArgsConstructor
    enum ChatType {
        SINGLE("0", "单聊"),
        GROUP("1", "群聊"),
        ;

        private String type;
        private String desc;
    }

}
