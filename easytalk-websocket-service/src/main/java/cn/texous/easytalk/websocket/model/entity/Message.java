package cn.texous.easytalk.websocket.model.entity;

import lombok.Data;

/**
 * insert description here
 *
 * @param <T> type
 * @author Showa.L
 * @since 2019/8/29 11:32
 */
@Data
public class Message<T> extends MsgType {

    private T detail;

}
