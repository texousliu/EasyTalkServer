package cn.texous.easytalk.websocket.strategy.message.redis;

import cn.texous.easytalk.websocket.config.ServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.tio.websocket.common.WsResponse;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/10 19:37
 */
public abstract class AbstractEtRedisMsgHander implements EtRedisMsgHandler {

    @Autowired
    private ServerConfig serverConfig;

    protected WsResponse getResponse(String text) {
        return WsResponse.fromText(text, serverConfig.getTioMsgCharset());
    }

}
