package cn.texous.easytalk.websocket.model.entity;

import cn.texous.easytalk.websocket.common.constants.ChatEnum;
import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/29 14:28
 */
@Data
public class ChatMsg {

    /**
     * 消息类型 参考{@link ChatEnum.ChatType ChatType}
     */
    private String type;
    /**
     * 消息的发送者id
     */
    private String fromid;
    /**
     * 消息的接收者，可以是群或者个人
     */
    private String toId;
    /**
     * 消息内容
     */
    private String content;

    /**
     * 发送者用户名
     */
    private String username;
    /**
     * 发送者头像
     */
    private String avatar;
    /**
     * 消息id
     */
    private String msgId;
    /**
     * 是否本人发送
     */
    private boolean mine;
    /**
     * 服务端时间戳毫秒数
     */
    private long timestamp;

}
