package cn.texous.easytalk.websocket.strategy.message.chat;

import org.springframework.stereotype.Component;
import org.tio.core.ChannelContext;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/28 14:42
 */
@Component(EtConnectMessageHandler.BEAN_NAME)
public class EtConnectMessageHandler extends AbstractEtMessageHandler {

    /** bean name */
    public static final String BEAN_NAME = "etConnectMessageHandler";

    @Override
    public Object handler(String msgText, ChannelContext channelContext) {
        //未读消息
        sendOffLineMessage(channelContext);
        return null;
    }

}
