package cn.texous.easytalk.websocket.context;

import cn.texous.easytalk.websocket.common.constants.ChatEnum;
import cn.texous.easytalk.websocket.strategy.message.chat.EtMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/28 14:47
 */
@Component
public class EtMessageHandlerContext {

    private static final Map<String, EtMessageHandler> ET_MESSAGE_HANDLER_MAP = new HashMap<>();

    @Autowired
    private void init(Map<String, EtMessageHandler> imMessageHandlerMap) {
        imMessageHandlerMap.forEach((k, v) ->
                ET_MESSAGE_HANDLER_MAP.put(ChatEnum.MsgType.loadByHandler(k), v)
        );
    }

    public EtMessageHandler loadByType(String type) {
        return ET_MESSAGE_HANDLER_MAP.get(type);
    }

}
