package cn.texous.easytalk.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.tio.websocket.starter.EnableTioWebSocketServer;

/**
 * main
 *
 * @author leven
 * @since
 */
@SpringBootApplication
@EnableFeignClients
@EnableTioWebSocketServer
public class EasytalkWebsocketServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasytalkWebsocketServiceApplication.class, args);
    }

}
