package cn.texous.easytalk.websocket.common.constants;

import cn.texous.easytalk.commonutil.constant.MsgTypeConst;
import cn.texous.easytalk.commonutil.exception.BusinessException;
import cn.texous.easytalk.websocket.strategy.message.redis.EtAddFriendConfirmRedisMsgHandler;
import cn.texous.easytalk.websocket.strategy.message.redis.EtAddFriendReqRedisMsgHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/30 13:44
 */
public interface RedisMsgEnum {

    /** 配置 redis message type */
    @Getter
    @AllArgsConstructor
    enum MsgType {
        ADD_FRIEND_REQ(MsgTypeConst.ADD_FRIEND_REQ,
                EtAddFriendReqRedisMsgHandler.BEAN_NAME, "好友申请"),
        ADD_FRIEND_COMFIRM(MsgTypeConst.ADD_FRIEND_COMFIRM,
                EtAddFriendConfirmRedisMsgHandler.BEAN_NAME, "同意好友申请"),
        ADD_GROUP_REQ(MsgTypeConst.ADD_GROUP_REQ, "", "入群申请"),
        ADD_GROUP_CONFIRM(MsgTypeConst.ADD_GROUP_CONFIRM, "", "同意入群申请"),
        INVITE_ADD_GROUP_REQ(MsgTypeConst.INVITE_ADD_GROUP_REQ, "", "邀请入群"),
        INVITE_ADD_GROUP_COMFIRM(MsgTypeConst.INVITE_ADD_GROUP_COMFIRM, "", "同意邀请入群"),
        DELETE_FRIENDS_REQ(MsgTypeConst.DELETE_FRIENDS_REQ, "", "删除好友"),

        ;

        private String type;
        private String handler;
        private String desc;

        public static String loadByHandler(String handler) {
            return Stream.of(ChatEnum.MsgType.values())
                    .filter(p -> p.getHandler().equals(handler))
                    .findFirst()
                    .map(ChatEnum.MsgType::getType)
                    .orElseThrow(() -> new BusinessException(WsCode.MESSAGE_HANDER_UNSUPPORT));
        }

    }

}
