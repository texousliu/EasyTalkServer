package cn.texous.easytalk.manager.dao;

import cn.texous.easytalk.manager.EasytalkManagerWebApplicationTests;
import cn.texous.easytalk.manager.db.BaseDbUnit;
import cn.texous.easytalk.manager.model.entity.EtChatGroup;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/24 11:01
 */
public class EtChatGroupMapperTest extends EasytalkManagerWebApplicationTests {

    @Resource
    private EtChatGroupMapper etChatGroupMapper;

    @Before
    public void init() {
        BaseDbUnit.initializeDatabase("h2_db/build/chat_group_init.sql");
    }

    @Test
    public void test() {
        EtChatGroup chatGroup = etChatGroupMapper.selectByPrimaryKey(1);
        Assert.assertEquals(chatGroup.getCode(), "123456");
    }

}