package ${basePackage}.service.api;

import ${basePackage}.model.entity.${modelNameUpperCamel};
import ${basePackage}.config.mysql.Service;


/**
 * insert description here.
 *
 * @author ${author}
 * @since  ${date}
 */
public interface ${modelNameUpperCamel}Service extends Service<${modelNameUpperCamel}> {

}
