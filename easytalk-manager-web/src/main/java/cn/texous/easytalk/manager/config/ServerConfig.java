package cn.texous.easytalk.manager.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/8/29 11:32
 */
@Data
@Component
@ConfigurationProperties(prefix = "texous.manager.config")
public class ServerConfig {

    private int clusterId;
    private String defaultHeader;
    private String defaultSign;
    private String defaultUserGroupName;
    private String serverName;
    private String serverVersion;

}
