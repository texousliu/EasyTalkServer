package cn.texous.easytalk.manager.publisher;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019-09-03 18:58
 */
public interface EtSubscribe {

    void onEvent(Object object);

    String getTopic();

}
