package cn.texous.easytalk.manager.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/23 22:07
 */
@Data
public class FindUserDto {

    @ApiModelProperty(
            required = true,
            notes = "用户 id， 由数据库生成",
            example = "123"
    )
    private Long id;

    @ApiModelProperty(
            required = true,
            notes = "用户编码, 用户唯一标识",
            example = "6234351234123434"
    )
    private String code;

    @ApiModelProperty(
            required = true,
            notes = "用户名",
            example = "texousliu"
    )
    private String username;

    @ApiModelProperty(
            notes = "用户昵称",
            example = "舔一天"
    )
    private String nickname;

    @ApiModelProperty(
            notes = "用户头像",
            example = "https://leven-test-bucket.nos-eastchina1.126.net/easytalk/default_header.jpeg"
    )
    private String avatar;

    @ApiModelProperty(
            notes = "用户签名",
            example = "what are you talking about"
    )
    private String sign;

}
