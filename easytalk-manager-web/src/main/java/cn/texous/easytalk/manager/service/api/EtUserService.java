package cn.texous.easytalk.manager.service.api;

import cn.texous.easytalk.manager.config.mysql.Service;
import cn.texous.easytalk.manager.model.dto.FindUserDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.dto.UserLoginDto;
import cn.texous.easytalk.manager.model.entity.EtUser;
import cn.texous.easytalk.manager.model.params.friend.AddFriendParam;
import cn.texous.easytalk.manager.model.params.friend.AgreeFriendAddParam;
import cn.texous.easytalk.manager.model.params.friend.DeleteFriendParam;
import cn.texous.easytalk.manager.model.params.friend.FindFriendByIdParam;
import cn.texous.easytalk.manager.model.params.friend.FindFriendByNameParam;
import cn.texous.easytalk.manager.model.params.friend.FriendGroupChangeParam;
import cn.texous.easytalk.manager.model.params.friend.FriendRemarkChangeParam;
import cn.texous.easytalk.manager.model.params.user.UserLoginParam;
import cn.texous.easytalk.manager.model.params.user.UserRegisterParam;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019-09-02 10:55:33
 */
public interface EtUserService extends Service<EtUser> {

    /**
     * 判断用户是否存在
     *
     * @param code code
     * @return
     */
    Boolean hasUser(String code);

    /**
     * 校验用户名是否存在
     *
     * @param username username
     * @return
     */
    Boolean existsUsername(String username);

    /**
     * 用户注册
     *
     * @param param param
     * @return
     */
    Boolean register(UserRegisterParam param);

    /**
     * 用户登录
     *
     * @param param param
     * @return
     */
    UserLoginDto login(UserLoginParam param);

    /**
     * 退出登陆
     *
     * @param token token
     * @param userDto userinfo
     * @return
     */
    boolean logout(String token, UserDto userDto);

    /**
     * 添加好友
     *
     * @param param param
     * @param userDto current user
     * @return
     */
    boolean addFriend(AddFriendParam param, UserDto userDto);

    /**
     * 同意好友添加
     *
     * @param param param
     * @param userDto current user
     * @return
     */
    boolean agreeFriendAdd(AgreeFriendAddParam param, UserDto userDto);

    /**
     * 删除好友
     *
     * @param param param
     * @param userDto current user
     * @return
     */
    boolean deleteFriends(DeleteFriendParam param, UserDto userDto);

    /**
     * 根据 userId 查找用户
     *
     * @param param param
     * @return
     */
    FindUserDto findUserById(FindFriendByIdParam param);

    /**
     * 根据 username 查找用户
     *
     * @param param param
     * @return
     */
    FindUserDto findUserByName(FindFriendByNameParam param);

    /**
     * 修改用户分组
     *
     * @param param param
     * @param userDto current user
     * @return
     */
    boolean changeFriendGroup(FriendGroupChangeParam param, UserDto userDto);

    /**
     * 修改用户备注
     *
     * @param param param
     * @param userDto current user
     * @return
     */
    boolean changeFriendRemark(FriendRemarkChangeParam param, UserDto userDto);

}
