package cn.texous.easytalk.manager.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * dto
 *
 * @author leven
 * @since
 */
@Data
public class UserGroupDto {

    @ApiModelProperty(
            required = true,
            notes = "分组 id",
            example = "123"
    )
    private Long id;

    @ApiModelProperty(
            required = true,
            notes = "分组编码",
            example = "66231232134132341"
    )
    private String code;

    @ApiModelProperty(
            required = true,
            notes = "分组名称",
            example = "好基友"
    )
    private String name;

}
