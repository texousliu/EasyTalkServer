package cn.texous.easytalk.manager.model.params.message;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/25 15:40
 */
@Data
public class ListChatMessageParam {

    @ApiModelProperty(
            required = true,
            notes = "聊天 code"
    )
    @NotNull
    private String chatCode;

    private Integer page;

    private Integer size;

    public Integer getPage() {
        return page == null ? 1 : page;
    }

    public Integer getSize() {
        return size == null ? 20 : size;
    }
}
