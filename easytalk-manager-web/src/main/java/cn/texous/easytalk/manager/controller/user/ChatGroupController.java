package cn.texous.easytalk.manager.controller.user;

import cn.texous.easytalk.commonutil.constant.Result;
import cn.texous.easytalk.manager.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 群聊操作 API
 *
 * @author Showa.L
 * @since 2019/9/2 11:26
 */
@Slf4j
@RestController
@RequestMapping("/chatgroup")
public class ChatGroupController extends BaseController {

    /**
     * 申请加入群聊
     * @param params 参数
     * @return
     */
    @PostMapping("/join")
    public Result joinChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 通过申请群聊加入
     * @param params 参数
     * @return
     */
    @PostMapping("/agree/join")
    public Result agreeChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 邀请加入
     * @param params 参数
     * @return
     */
    @PostMapping("/invite/add")
    public Result inviteAddChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 同意邀请
     * @param params 参数
     * @return
     */
    @PostMapping("/invite/agree")
    public Result inviteAgreeChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 踢人
     * @param params 参数
     * @return
     */
    @PostMapping("/kicking")
    public Result kickingChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 新建群聊
     * @param params 参数
     * @return
     */
    @PostMapping("/add")
    public Result addChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }


    /**
     * 删除群聊
     * @param params 参数
     * @return
     */
    @PostMapping("/delete")
    public Result deleteChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 拉黑群聊
     * @param params 参数
     * @return
     */
    @PostMapping("/black")
    public Result blackChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 屏蔽群聊
     * @param params 参数
     * @return
     */
    @PostMapping("/shield")
    public Result shieldChatGroup(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 根据id查找群聊
     * @param params 参数
     * @return
     */
    @PostMapping("/find/id")
    public Result findFriendById(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    /**
     * 根据名称查找群聊
     * @param params 参数
     * @return
     */
    @PostMapping("/find/name")
    public Result findFriendByName(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

}
