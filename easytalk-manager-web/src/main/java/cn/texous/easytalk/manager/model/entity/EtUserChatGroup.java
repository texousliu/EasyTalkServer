package cn.texous.easytalk.manager.model.entity;

import java.util.Date;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 实体类
 */
@Data
@Table(name = "et_user_chat_group")
public class EtUserChatGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * et_user 表 code
     */
    @Column(name = "user_code")
    private String userCode;

    /**
     * et_chat_group 表 code
     */
    @Column(name = "chat_group_code")
    private String chatGroupCode;

    /**
     * 用户群昵称
     */
    private String nickname;

    /**
     * 状态：0=待生效，1=正常，4=删除
     */
    private Integer status;

    /**
     * 入群时间
     */
    private Date created;

    private Date updated;

}
