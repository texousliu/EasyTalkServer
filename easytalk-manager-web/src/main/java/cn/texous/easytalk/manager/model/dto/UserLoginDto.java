package cn.texous.easytalk.manager.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/3 20:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginDto {

    @ApiModelProperty(
            required = true,
            notes = "user login success token, used when connect to socket",
            example = "sdfadfasdfasdf"
    )
    private String token;
    @ApiModelProperty(
            required = true,
            notes = "login user info"
    )
    private UserDto user;
    @ApiModelProperty(
            notes = "user chat group list"
    )
    private List<UserChatGroupDto> groups;

}
