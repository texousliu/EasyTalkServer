package cn.texous.easytalk.manager.model.params.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/2 15:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterParam {

    /**
     * 用户名
     */
    @ApiModelProperty(
            required = true,
            notes = "用户名",
            example = "texousliu"
    )
    @NotNull(message = "用户名不能为空")
    @Length(min = 2, max = 25, message = "用户名长度为 2~25 位")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(
            required = true,
            notes = "密码",
            example = "123456"
    )
    @NotNull(message = "密码不能为空")
    @Length(min = 6, max = 25, message = "密码长度为 6~25 位")
    private String password;

    /**
     * 昵称
     */
    @ApiModelProperty(
            required = true,
            notes = "昵称",
            example = "Showa.L"
    )
    @NotNull(message = "昵称不能为空")
    @Length(min = 1, max = 25, message = "昵称长度为 1~25 位")
    private String nickname;

    /**
     * 头像
     */
    @ApiModelProperty(
            notes = "用户头像",
            example = "https://leven-test-bucket.nos-eastchina1.126.net/easytalk/default_header.jpeg"
    )
    private String avatar;

    /**
     * 签名
     */
    @ApiModelProperty(
            notes = "用户签名",
            example = "没有什么能够阻挡，我对自由的向往"
    )
    private String sign;

    /**
     * 手机号
     */
    @ApiModelProperty(
            notes = "手机号",
            example = "189******86"
    )
    private String mobile;

    /**
     * 邮箱
     */
    @ApiModelProperty(
            notes = "邮箱",
            example = "123456789@qq.com"
    )
    private String email;

    /**
     * 备注
     */
    @ApiModelProperty(
            notes = "备注",
            example = "这个人很懒，什么都没留下"
    )
    private String remarks;

}
