package cn.texous.easytalk.manager.model.params.friend;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:57
 */
@Data
public class DeleteFriendParam {

    @ApiModelProperty(
            required = true,
            notes = "需要删除的好友的 code 列表",
            example = "[684833535206494212]"
    )
    @NotNull
    private List<String> codes;

}
