package cn.texous.easytalk.manager.service.api;

import cn.texous.easytalk.manager.config.mysql.Service;
import cn.texous.easytalk.manager.model.dto.FindUserDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.dto.UserGroupDto;
import cn.texous.easytalk.manager.model.entity.EtUserGroup;
import cn.texous.easytalk.manager.model.params.userGroup.AddUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.DeleteUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.UpdateUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.UserGroupUsersParam;

import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since  2019-09-02 10:55:33
 */
public interface EtUserGroupService extends Service<EtUserGroup> {

    UserGroupDto addUserGroup(AddUserGroupParam param, UserDto userDto);

    boolean updateUserGroupName(UpdateUserGroupParam param, UserDto userDto);

    boolean deleteUserGroup(DeleteUserGroupParam param, UserDto userDto);

    List<UserGroupDto> listUserGroup(UserDto userDto);

    List<FindUserDto> listGroupUsers(UserGroupUsersParam param, UserDto userDto);

}
