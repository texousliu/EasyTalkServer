package cn.texous.easytalk.manager.model.params.friend;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:57
 */
@Data
public class FindFriendByNameParam {

    @ApiModelProperty(
            required = true,
            notes = "需要查找的用户名",
            example = "texousliu"
    )
    @NotNull
    private String username;

}
