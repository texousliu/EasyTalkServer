package cn.texous.easytalk.manager.model.entity;

import java.util.Date;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * entity
 */
@Data
@Table(name = "et_user_read")
public class EtUserRead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 用户id
     */
    @Column(name = "user_code")
    private String userCode;

    /**
     * 聊天id：user_id 或 chat_group_id
     */
    @Column(name = "chat_code")
    private String chatCode;

    /**
     * 最新阅读消息id
     */
    @Column(name = "message_code")
    private String messageCode;

    /**
     * 最新阅读时间
     */
    @Column(name = "last_time")
    private Date lastTime;

    /**
     * 更新时间
     */
    private Date updated;

}
