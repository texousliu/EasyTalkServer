package cn.texous.easytalk.manager.publisher;

import cn.texous.easytalk.commonutil.constant.MsgTypeConst;
import cn.texous.easytalk.commonutil.constant.RedisKey;
import cn.texous.easytalk.commonutil.model.redis.RedisMsg;
import cn.texous.easytalk.commonutil.model.redis.RedisMsgDetail;
import cn.texous.easytalk.commonutil.util.GsonUtils;
import cn.texous.starter.redis.RedisClient;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/23 18:01
 */
public class RedisPublisher {

    /**
     * 添加好友请求
     * @param fromCode 发起人
     * @param toCode 需要添加的好友
     * @param fromName 发起人名称
     * @return
     */
    public static boolean addFriend(String fromCode, String toCode, String fromName) {
        RedisMsg redisMsg = RedisMsg.builder()
                .type(MsgTypeConst.ADD_FRIEND_REQ)
                .detail(RedisMsgDetail.builder()
                        .fromCode(fromCode)
                        .toCodes(Lists.newArrayList(toCode))
                        .fromName(fromName)
                        .build())
                .build();
        RedisClient.publish(RedisKey.CHANNEL_MANAGER_SYSTEM_EVENT_MSG, GsonUtils.toJson(redisMsg));
        return true;
    }

    /**
     * 同意好友添加请求
     *
     * @param fromCode 通过请求的人
     * @param toCode   发起请求的人
     * @param fromName 名称
     * @return
     */
    public static boolean agreeFriendAdd(String fromCode, String toCode, String fromName) {
        RedisMsg redisMsg = RedisMsg.builder()
                .type(MsgTypeConst.ADD_FRIEND_COMFIRM)
                .detail(RedisMsgDetail.builder()
                        .fromCode(fromCode)
                        .toCodes(Lists.newArrayList(toCode))
                        .fromName(fromName)
                        .build())
                .build();
        RedisClient.publish(RedisKey.CHANNEL_MANAGER_SYSTEM_EVENT_MSG, GsonUtils.toJson(redisMsg));
        return true;
    }

    /**
     * 删除好友
     * @param fromCode 操作人
     * @param toCodes 被删除的用户
     * @param fromName 操作人
     * @return
     */
    public static boolean deleteFriends(String fromCode, List<String> toCodes, String fromName) {
        RedisMsg redisMsg = RedisMsg.builder()
                .type(MsgTypeConst.DELETE_FRIENDS_REQ)
                .detail(RedisMsgDetail.builder()
                        .fromCode(fromCode)
                        .toCodes(toCodes)
                        .fromName(fromName)
                        .build())
                .build();
        RedisClient.publish(RedisKey.CHANNEL_MANAGER_SYSTEM_EVENT_MSG, GsonUtils.toJson(redisMsg));
        return true;
    }

}
