package cn.texous.easytalk.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 主启动类
 */
@SpringBootApplication
@EnableSwagger2
public class EasytalkManagerWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasytalkManagerWebApplication.class, args);
    }

}
