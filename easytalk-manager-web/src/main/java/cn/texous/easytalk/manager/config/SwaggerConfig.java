package cn.texous.easytalk.manager.config;

import cn.texous.easytalk.commonutil.constant.Result;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.lang.reflect.WildcardType;

/**
 * Created by Alexey Malyshev on 25.06.2018
 */
@Configuration
public class SwaggerConfig {

    private static final String ROOT_CONTROLLER = "cn.texous.easytalk.manager.controller";
    private static final String URI_APACHE = "https://www.apache.org/licenses/LICENSE-2.0";

    @Autowired
    private ServerConfig serverConfig;
    @Autowired
    private TypeResolver typeResolver;

    /**
     * docket api 配置
     * @return
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(ROOT_CONTROLLER))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo(serverConfig.getServerName(), serverConfig.getServerVersion()))
                .alternateTypeRules(
                        AlternateTypeRules.newRule(
                                typeResolver.resolve(Result.class, WildcardType.class),
                                typeResolver.resolve(WildcardType.class)));
    }

    private ApiInfo apiInfo(String title, String version) {
        return new ApiInfoBuilder()
                .title("REST API " + title)
                .version("Version " + version)
                .license("Apache License Version 2.0")
                .licenseUrl(URI_APACHE)
                .build();
    }

}
