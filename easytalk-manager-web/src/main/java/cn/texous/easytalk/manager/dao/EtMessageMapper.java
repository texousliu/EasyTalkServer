package cn.texous.easytalk.manager.dao;

import cn.texous.easytalk.manager.config.mysql.MyMapper;
import cn.texous.easytalk.manager.model.entity.EtMessage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * mapper
 */
@Mapper
public interface EtMessageMapper extends MyMapper<EtMessage> {

    List<EtMessage> selectByChatCode(@Param("chatCode") String chatCode,
                                     @Param("status") Integer status);

}
