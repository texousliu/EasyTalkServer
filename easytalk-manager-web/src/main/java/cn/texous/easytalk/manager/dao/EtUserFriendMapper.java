package cn.texous.easytalk.manager.dao;

import cn.texous.easytalk.manager.config.mysql.MyMapper;
import cn.texous.easytalk.manager.model.entity.EtUser;
import cn.texous.easytalk.manager.model.entity.EtUserFriend;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * mapper
 * TODO 需要建立聚合索引 user_code 和 friend_code， user_code 和 user_group_code
 */
@Mapper
public interface EtUserFriendMapper extends MyMapper<EtUserFriend> {

    int existsFriend(@Param("friendCode") String friendCode,
                     @Param("userCode") String userCode,
                     @Param("status") Integer status);

    int updateStatus(@Param("friendCode") String friendCode,
                     @Param("userCode") String userCode,
                     @Param("status") Integer status);

    int updateStatusBatch(@Param("friendCodes") List<String> friendCodes,
                          @Param("userCode") String userCode,
                          @Param("status") Integer status);

    int updateUserGroup(@Param("userGroupCode") String userGroupCode,
                        @Param("friendCode") String friendCode,
                        @Param("userCode") String userCode);

    int updateUserRemark(@Param("remarkName") String remarkName,
                         @Param("friendCode") String friendCode,
                         @Param("userCode") String userCode);

    int updateOldGroupToNew(@Param("oldUserGroupCode") String oldUserGroupCode,
                            @Param("newUserGroupCode") String newUserGroupCode,
                            @Param("userCode") String userCode);

    List<EtUser> listGroupUsers(@Param("userGroupCode") String userGroupCode,
                                @Param("userCode") String userCode);

}
