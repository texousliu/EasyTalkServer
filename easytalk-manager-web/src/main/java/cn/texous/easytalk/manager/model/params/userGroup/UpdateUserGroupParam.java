package cn.texous.easytalk.manager.model.params.userGroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/2 15:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserGroupParam {

    @ApiModelProperty(
            required = true,
            notes = "分组编码",
            example = "63421343452345"
    )
    @NotNull
    private String code;

    @ApiModelProperty(
            required = true,
            notes = "新分组名",
            example = "真基友"
    )
    @NotNull
    private String name;

}
