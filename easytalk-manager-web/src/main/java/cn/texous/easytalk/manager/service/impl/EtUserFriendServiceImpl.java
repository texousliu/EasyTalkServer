package cn.texous.easytalk.manager.service.impl;

import cn.texous.easytalk.manager.dao.EtUserFriendMapper;
import cn.texous.easytalk.manager.model.entity.EtUserFriend;
import cn.texous.easytalk.manager.service.api.EtUserFriendService;
import cn.texous.easytalk.manager.config.mysql.AbstractService;
import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since  2019-09-02 10:55:33
 */
@Service
// @Transactional
public class EtUserFriendServiceImpl
        extends AbstractService<EtUserFriend> implements EtUserFriendService {

    @Resource
    private EtUserFriendMapper etUserFriendMapper;

    @Override
    public boolean existsFriend(String friendCode, String userCode, Integer status) {
        return etUserFriendMapper.existsFriend(friendCode, userCode, status) > 0;
    }
}
