package cn.texous.easytalk.manager.dao;

import cn.texous.easytalk.manager.config.mysql.MyMapper;
import cn.texous.easytalk.manager.model.entity.EtUserGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * mapper
 * TODO 需要建唯一索引，user_code 和 name， 聚合索引 user_code 和 code
 */
@Mapper
public interface EtUserGroupMapper extends MyMapper<EtUserGroup> {

    int existsName(@Param("name") String name, @Param("userCode") String userCode);

    EtUserGroup selectByName(@Param("name") String name, @Param("userCode") String userCode);

    List<EtUserGroup> selectByUserCode(@Param("userCode") String userCode);

    int updateGroupName(@Param("groupName") String groupName,
                        @Param("groupCode") String groupCode,
                        @Param("userCode") String userCode);

    int deleteByCode(@Param("groupCode") String groupCode, @Param("userCode") String userCode);

}
