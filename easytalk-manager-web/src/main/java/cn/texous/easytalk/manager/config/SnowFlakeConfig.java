package cn.texous.easytalk.manager.config;

import cn.texous.easytalk.commonutil.util.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * snow flake config
 *
 * @author leven
 * @since
 */
@Slf4j
@Configuration
public class SnowFlakeConfig {

    @Autowired
    private ServerConfig serverConfig;

    @Bean("userCodeSnowFlake")
    public SnowFlake userCodeSnowFlake() {
        return new SnowFlake(serverConfig.getClusterId(), 1);
    }

    @Bean("userGroupCodeSnowFlake")
    public SnowFlake userGroupCodeSnowFlake() {
        return new SnowFlake(serverConfig.getClusterId(), 2);
    }

}
