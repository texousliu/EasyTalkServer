package cn.texous.easytalk.manager.dao;

import cn.texous.easytalk.manager.config.mysql.MyMapper;
import cn.texous.easytalk.manager.model.entity.EtUser;
import cn.texous.easytalk.manager.model.vo.UserChatGroupVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * mapper
 */
@Mapper
public interface EtUserMapper extends MyMapper<EtUser> {

    int existsUsername(@Param("username") String username);

    int selectByCode(@Param("code") String code, @Param("status") Integer status);

    EtUser selectByUsername(@Param("username") String username, @Param("status") Integer status);

    List<UserChatGroupVO> selectUserChatGroup(@Param("userCode") String userCode,
                                              @Param("status") Integer status);

}
