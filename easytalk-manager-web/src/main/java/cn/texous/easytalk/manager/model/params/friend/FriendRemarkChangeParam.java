package cn.texous.easytalk.manager.model.params.friend;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:57
 */
@Data
public class FriendRemarkChangeParam {

    @ApiModelProperty(
            required = true,
            notes = "用户code",
            example = "123"
    )
    @NotNull
    private String code;

    @ApiModelProperty(
            required = true,
            notes = "分组 code",
            example = "这是个基友"
    )
    @NotNull
    private String remarkName;

}
