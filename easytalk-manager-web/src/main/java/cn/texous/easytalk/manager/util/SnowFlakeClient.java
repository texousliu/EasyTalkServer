package cn.texous.easytalk.manager.util;

import cn.texous.easytalk.commonutil.util.SnowFlake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/2 16:04
 */
@Component
public class SnowFlakeClient {

    private static SnowFlake userCode;
    private static SnowFlake userGroupCode;

    @Autowired
    private SnowFlake userCodeSnowFlake;
    @Autowired
    private SnowFlake userGroupCodeSnowFlake;

    @PostConstruct
    private void init() {
        userCode = userCodeSnowFlake;
        userGroupCode = userGroupCodeSnowFlake;
    }

    public static String userCode() {
        return String.valueOf(userCode.nextId());
    }

    public static String userGroupCode() {
        return String.valueOf(userGroupCode.nextId());
    }

}
