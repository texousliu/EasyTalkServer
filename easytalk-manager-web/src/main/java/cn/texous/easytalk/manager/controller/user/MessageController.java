package cn.texous.easytalk.manager.controller.user;

import cn.texous.easytalk.commonutil.constant.Result;
import cn.texous.easytalk.manager.controller.BaseController;
import cn.texous.easytalk.manager.model.dto.MessageDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.params.message.ListChatMessageParam;
import cn.texous.easytalk.manager.service.api.EtMessageService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 聊天 API
 *
 * @author Showa.L
 * @since 2019/9/2 12:01
 */
@Api(value = "聊天消息管理", tags = "聊天消息管理接口")
@Slf4j
@RestController
@RequestMapping("/message")
public class MessageController extends BaseController {

    @Autowired
    private EtMessageService etMessageService;

    @PostMapping("/list/code")
    @ApiOperation(value = "获取某个聊天的聊天记录")
    public Result<Page<MessageDto>> listChatMessage(
            @RequestBody @Valid ListChatMessageParam param) {
        UserDto userDto = currentUserNotNull();
        // TODO 需要进行用户 是否具有该消息的 查看权限 校验
        PageInfo<MessageDto> messageDtos = etMessageService.listChatMessage(param, userDto);
        return Result.ok(messageDtos);
    }

}
