package cn.texous.easytalk.manager.dao;

import cn.texous.easytalk.manager.config.mysql.MyMapper;
import cn.texous.easytalk.manager.model.entity.EtUserChatGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * mapper
 */
@Mapper
public interface EtUserChatGroupMapper extends MyMapper<EtUserChatGroup> {
}
