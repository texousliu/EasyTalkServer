package cn.texous.easytalk.manager.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:10
 */
@Data
public class UserChatGroupDto {

    @ApiModelProperty(
            required = true,
            notes = "群聊id，由数据库生成"
    )
    private Long chatGroupId;

    @ApiModelProperty(
            required = true,
            notes = "群聊编码，群聊唯一标识"
    )
    private String chatGroupCode;

    @ApiModelProperty(
            required = true,
            notes = "群聊名称"
    )
    private String chatGroupName;

    @ApiModelProperty(
            required = true,
            notes = "群聊头像"
    )
    private String chatGroupAvatar;

    @ApiModelProperty(
            required = true,
            notes = "群主唯一编码"
    )
    private String chatGroupMaster;

    @ApiModelProperty(
            notes = "群聊说明"
    )
    private String chatGroupRemarks;

    @ApiModelProperty(
            notes = "当前用户编码"
    )
    private String userCode;

    @ApiModelProperty(
            notes = "当前用户在群聊中的名称"
    )
    private String nickname;

    @ApiModelProperty(
            notes = "当前用户状态：0=待生效，1=正常，4=删除"
    )
    private Integer status;

}
