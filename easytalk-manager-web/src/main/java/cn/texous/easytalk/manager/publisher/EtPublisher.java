package cn.texous.easytalk.manager.publisher;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019-09-03 18:55
 */
@Component
public class EtPublisher {

    private static final ConcurrentHashMap<String,
            CopyOnWriteArrayList<EtSubscribe>> SUB_MAP = new ConcurrentHashMap<>();

    /**
     * 订阅事件
     * @param subscribe subscribe
     * @return
     */
    public boolean subscribe(EtSubscribe subscribe) {
        if (SUB_MAP.get(subscribe.getTopic()) == null)
            SUB_MAP.put(subscribe.getTopic(), new CopyOnWriteArrayList<>());
        SUB_MAP.get(subscribe.getTopic()).add(subscribe);
        return true;
    }

    /**
     * 取消订阅
     * @param subscribe subscribe
     * @param topic 主题
     * @return
     */
    public boolean unSubscribe(EtSubscribe subscribe, String topic) {
        if (SUB_MAP.get(topic) != null)
            SUB_MAP.get(topic).remove(topic);
        return true;
    }

    /**
     * 发布消息
     * @param object 数据
     * @param topic 主题
     */
    public void publisher(Object object, String topic) {
        if (SUB_MAP.get(topic) != null)
            SUB_MAP.get(topic).forEach(p -> p.onEvent(object));
    }

}
