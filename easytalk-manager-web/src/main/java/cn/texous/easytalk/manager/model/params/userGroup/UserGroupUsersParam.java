package cn.texous.easytalk.manager.model.params.userGroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/2 15:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserGroupUsersParam {

    @ApiModelProperty(
            required = true,
            notes = "分组编码",
            example = "34131234524351243"
    )
    @NotNull
    private String code;

}
