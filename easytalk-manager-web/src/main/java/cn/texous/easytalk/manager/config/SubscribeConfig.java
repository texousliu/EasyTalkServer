package cn.texous.easytalk.manager.config;

import cn.texous.easytalk.manager.publisher.EtPublisher;
import cn.texous.easytalk.manager.publisher.EtSubscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Optional;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/3 20:15
 */
@Configuration
public class SubscribeConfig {

    @Autowired
    private EtPublisher etPublisher;

    @Autowired
    public void subscribe(Map<String, EtSubscribe> stringEtSubscribeMap) {
        Optional.ofNullable(stringEtSubscribeMap.entrySet())
                .ifPresent(p -> p.forEach(item -> etPublisher.subscribe(item.getValue())));
    }

}
