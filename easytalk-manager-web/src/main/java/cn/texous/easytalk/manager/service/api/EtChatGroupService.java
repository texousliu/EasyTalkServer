package cn.texous.easytalk.manager.service.api;

import cn.texous.easytalk.manager.model.entity.EtChatGroup;
import cn.texous.easytalk.manager.config.mysql.Service;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since  2019-09-02 10:55:33
 */
public interface EtChatGroupService extends Service<EtChatGroup> {

}
