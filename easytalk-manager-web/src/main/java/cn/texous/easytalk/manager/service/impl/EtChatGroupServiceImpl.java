package cn.texous.easytalk.manager.service.impl;

import cn.texous.easytalk.manager.dao.EtChatGroupMapper;
import cn.texous.easytalk.manager.model.entity.EtChatGroup;
import cn.texous.easytalk.manager.service.api.EtChatGroupService;
import cn.texous.easytalk.manager.config.mysql.AbstractService;
import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since  2019-09-02 10:55:33
 */
@Service
// @Transactional
public class EtChatGroupServiceImpl
        extends AbstractService<EtChatGroup>
        implements EtChatGroupService {
    @Resource
    private EtChatGroupMapper etChatGroupMapper;

}
