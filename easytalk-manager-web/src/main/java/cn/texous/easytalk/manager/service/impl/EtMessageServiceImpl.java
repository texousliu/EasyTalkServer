package cn.texous.easytalk.manager.service.impl;

import cn.texous.easytalk.manager.config.mysql.AbstractService;
import cn.texous.easytalk.manager.dao.EtMessageMapper;
import cn.texous.easytalk.manager.model.dto.MessageDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.entity.EtMessage;
import cn.texous.easytalk.manager.model.params.message.ListChatMessageParam;
import cn.texous.easytalk.manager.service.api.EtMessageService;
import cn.texous.easytalk.manager.util.PageInfoConverterUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

// import org.springframework.transaction.annotation.Transactional;
import java.util.List;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019-09-02 10:55:33
 */
@Service
// @Transactional
public class EtMessageServiceImpl
        extends AbstractService<EtMessage>
        implements EtMessageService {

    @Resource
    private EtMessageMapper etMessageMapper;

    @Override
    public PageInfo<MessageDto> listChatMessage(ListChatMessageParam param, UserDto userDto) {
        PageHelper.startPage(param.getPage(), param.getSize());
        List<EtMessage> messagePageInfo =
                etMessageMapper.selectByChatCode(param.getChatCode(), null);
        List<MessageDto> resultList = PageInfoConverterUtils
                .convertPage(MessageDto.class, messagePageInfo);
        return new PageInfo<>(resultList);
    }
}
