package cn.texous.easytalk.manager.model.entity;

import java.util.Date;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 实体类
 */
@Data
@Table(name = "et_chat_group")
public class EtChatGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    /**
     * 群名称
     */
    private String name;

    /**
     * 群头像
     */
    private String avatar;

    /**
     * 群主
     */
    private String master;

    /**
     * 说明
     */
    private String remarks;

    /**
     * 状态：0=待激活，1=正常，2=冻结，3=注销，4=已删除
     */
    private Integer status;

    private Date created;

    @Column(name = "created_by")
    private String createdBy;

    private Date updated;

    @Column(name = "updated_by")
    private String updatedBy;
}
