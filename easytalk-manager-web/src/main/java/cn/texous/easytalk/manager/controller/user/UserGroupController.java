package cn.texous.easytalk.manager.controller.user;

import cn.texous.easytalk.commonutil.constant.Result;
import cn.texous.easytalk.manager.controller.BaseController;
import cn.texous.easytalk.manager.model.dto.FindUserDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.dto.UserGroupDto;
import cn.texous.easytalk.manager.model.params.userGroup.AddUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.DeleteUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.UpdateUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.UserGroupUsersParam;
import cn.texous.easytalk.manager.service.api.EtUserGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * 好友分组 API
 *
 * @author Showa.L
 * @since 2019/9/2 11:26
 */
@Slf4j
@RestController
@RequestMapping("/usergroup")
@Api(value = "好友分组操作", tags = "好友分组操作接口")
public class UserGroupController extends BaseController {

    @Autowired
    private EtUserGroupService etUserGroupService;

    @PostMapping("/add")
    @ApiOperation(value = "添加好友分组")
    public Result<UserGroupDto> addUserGroup(@RequestBody @Valid AddUserGroupParam param) {
        UserDto userDto = currentUserNotNull();
        UserGroupDto result = etUserGroupService.addUserGroup(param, userDto);
        return Result.ok(result);
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除好友分组")
    public Result<Boolean> deleteUserGroup(@RequestBody @Valid DeleteUserGroupParam param) {
        UserDto userDto = currentUserNotNull();
        boolean result = etUserGroupService.deleteUserGroup(param, userDto);
        return Result.ok(result);
    }

    @PostMapping("/change/name")
    @ApiOperation(value = "修改好友分组名称")
    public Result<Boolean> changeUserGroupName(@RequestBody @Valid UpdateUserGroupParam param) {
        UserDto userDto = currentUserNotNull();
        boolean result = etUserGroupService.updateUserGroupName(param, userDto);
        return Result.ok(result);
    }

    @PostMapping("/list")
    @ApiOperation(value = "获取好友分组列表")
    public Result<List<UserGroupDto>> listUserGroup() {
        UserDto userDto = currentUserNotNull();
        List<UserGroupDto> userGroupDtos = etUserGroupService.listUserGroup(userDto);
        return Result.ok(userGroupDtos);
    }

    @PostMapping("/list/users")
    @ApiOperation(value = "获取好友分组中的用户列表")
    public Result<List<FindUserDto>> listGroupUsers(
            @RequestBody @Valid UserGroupUsersParam param) {
        UserDto userDto = currentUserNotNull();
        List<FindUserDto> findUserDtos = etUserGroupService.listGroupUsers(param, userDto);
        return Result.ok(findUserDtos);
    }

}
