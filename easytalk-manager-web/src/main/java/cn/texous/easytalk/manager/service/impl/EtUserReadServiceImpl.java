package cn.texous.easytalk.manager.service.impl;

import cn.texous.easytalk.manager.dao.EtUserReadMapper;
import cn.texous.easytalk.manager.model.entity.EtUserRead;
import cn.texous.easytalk.manager.service.api.EtUserReadService;
import cn.texous.easytalk.manager.config.mysql.AbstractService;
import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since  2019-09-02 10:55:33
 */
@Service
// @Transactional
public class EtUserReadServiceImpl
        extends AbstractService<EtUserRead> implements EtUserReadService {

    @Resource
    private EtUserReadMapper etUserReadMapper;

}
