package cn.texous.easytalk.manager.service.impl;

import cn.texous.easytalk.commonutil.util.ConverterUtils;
import cn.texous.easytalk.manager.config.ServerConfig;
import cn.texous.easytalk.manager.config.mysql.AbstractService;
import cn.texous.easytalk.manager.dao.EtUserFriendMapper;
import cn.texous.easytalk.manager.dao.EtUserGroupMapper;
import cn.texous.easytalk.manager.model.dto.FindUserDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.dto.UserGroupDto;
import cn.texous.easytalk.manager.model.entity.EtUser;
import cn.texous.easytalk.manager.model.entity.EtUserGroup;
import cn.texous.easytalk.manager.model.params.userGroup.AddUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.DeleteUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.UpdateUserGroupParam;
import cn.texous.easytalk.manager.model.params.userGroup.UserGroupUsersParam;
import cn.texous.easytalk.manager.service.api.EtUserGroupService;
import cn.texous.easytalk.manager.util.SnowFlakeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019-09-02 10:55:33
 */
@Service
public class EtUserGroupServiceImpl
        extends AbstractService<EtUserGroup> implements EtUserGroupService {

    @Resource
    private EtUserGroupMapper etUserGroupMapper;
    @Resource
    private EtUserFriendMapper etUserFriendMapper;
    @Autowired
    private ServerConfig serverConfig;

    @Override
    public UserGroupDto addUserGroup(AddUserGroupParam param, UserDto userDto) {
        EtUserGroup etUserGroup = new EtUserGroup();
        etUserGroup.setCode(SnowFlakeClient.userGroupCode());
        etUserGroup.setUserCode(userDto.getCode());
        etUserGroup.setName(param.getName());
        etUserGroupMapper.insertSelective(etUserGroup);
        return ConverterUtils.convert(UserGroupDto.class, etUserGroup);
    }

    @Override
    public boolean updateUserGroupName(UpdateUserGroupParam param, UserDto userDto) {
        etUserGroupMapper.updateGroupName(param.getName(), param.getCode(), userDto.getCode());
        return true;
    }

    @Override
    @Transactional
    public boolean deleteUserGroup(DeleteUserGroupParam param, UserDto userDto) {
        String defaultGroupName = serverConfig.getDefaultUserGroupName();
        // 获取默认分组
        EtUserGroup userGroup = etUserGroupMapper
                .selectByName(defaultGroupName, userDto.getCode());
        // 更新分组用户关联到默认分组
        etUserFriendMapper.updateOldGroupToNew(
                param.getCode(), userGroup.getCode(), userDto.getCode());
        // 删除分组
        etUserGroupMapper.deleteByCode(param.getCode(), userDto.getCode());
        return true;
    }

    @Override
    public List<UserGroupDto> listUserGroup(UserDto userDto) {
        List<EtUserGroup> userGroups = etUserGroupMapper.selectByUserCode(userDto.getCode());
        return ConverterUtils.convert(UserGroupDto.class, userGroups);
    }

    @Override
    public List<FindUserDto> listGroupUsers(UserGroupUsersParam param, UserDto userDto) {
        List<EtUser> users = etUserFriendMapper.listGroupUsers(param.getCode(), userDto.getCode());
        return ConverterUtils.convert(FindUserDto.class, users);
    }
}
