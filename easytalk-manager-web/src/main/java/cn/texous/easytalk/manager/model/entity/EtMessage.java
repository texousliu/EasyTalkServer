package cn.texous.easytalk.manager.model.entity;

import java.util.Date;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 实体类
 */
@Data
@Table(name = "et_message")
public class EtMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 消息编码
     */
    private String code;

    /**
     * 聊天id：user_code 或 chat_group_code
     */
    @Column(name = "chat_code")
    private String chatCode;

    /**
     * et_user 表 code
     */
    @Column(name = "from_code")
    private String fromCode;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 聊天类型：0=单聊， 1=群聊
     */
    private Integer type;

    /**
     * 状态：0=待发送，1=已发送，2=发送失败，已撤销，4=已删除
     */
    private Integer status;

    @Column(name = "send_time")
    private Date sendTime;

    private Date created;
}
