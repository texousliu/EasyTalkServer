package cn.texous.easytalk.manager.model.entity;

import java.util.Date;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 实体类
 */
@Data
@Table(name = "et_user_friend")
public class EtUserFriend {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 用户code：et_user 表 code
     */
    @Column(name = "user_code")
    private String userCode;

    /**
     * 好友code：et_user 表 code
     */
    @Column(name = "friend_code")
    private String friendCode;

    /**
     * 用户分组：et_user_group 表 code
     */
    @Column(name = "user_group_code")
    private String userGroupCode;

    /**
     * 备注名
     */
    @Column(name = "remark_name")
    private String remarkName;

    /**
     * 状态：0=待确认，1=正常，2=屏蔽，3=拉黑，4=已删除
     */
    private Integer status;

    private Date created;

    @Column(name = "created_by")
    private String createdBy;

    private Date updated;

    @Column(name = "updated_by")
    private String updatedBy;

}
