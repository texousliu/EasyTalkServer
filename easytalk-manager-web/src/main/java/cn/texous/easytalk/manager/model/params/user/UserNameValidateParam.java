package cn.texous.easytalk.manager.model.params.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/6 18:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserNameValidateParam {

    @ApiModelProperty(
            required = true,
            notes = "用户名",
            example = "texousliu"
    )
    private String username;

}
