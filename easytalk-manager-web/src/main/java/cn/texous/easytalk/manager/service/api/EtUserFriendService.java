package cn.texous.easytalk.manager.service.api;

import cn.texous.easytalk.manager.model.entity.EtUserFriend;
import cn.texous.easytalk.manager.config.mysql.Service;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since  2019-09-02 10:55:33
 */
public interface EtUserFriendService extends Service<EtUserFriend> {

    /**
     * 查询好友关系是否存在
     * @param friendCode friend code
     * @param userCode user code
     * @param status status
     * @return
     */
    boolean existsFriend(String friendCode, String userCode, Integer status);

}
