package cn.texous.easytalk.manager.model.params.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * insert description here
 *
 * @author leven
 * @since 2019/9/3 19:32
 */
@Data
public class UserLoginParam {

    @ApiModelProperty(
            required = true,
            notes = "用户名",
            example = "texousliu"
    )
    private String username;
    @ApiModelProperty(
            required = true,
            notes = "密码",
            example = "123456789"
    )
    private String password;

}
