package cn.texous.easytalk.manager.util;

import cn.texous.easytalk.commonutil.util.ConverterUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;

import java.util.Iterator;
import java.util.List;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/5/5 14:03
 */
public class PageInfoConverterUtils {

    /**
     * pageinfo 分页数据转换
     *
     * @param tClass 目标类型
     * @param dList  源数据
     * @param <T>    目标类型
     * @param <D>    源类型
     * @return
     */
    public static <T, D> List<T> convertPage(Class<T> tClass, List<D> dList) {
        if (dList instanceof Page) {
            Page<T> result = new Page<>();
            if (!dList.isEmpty()) {
                Iterator<D> var3 = dList.iterator();
                while (var3.hasNext()) {
                    D obj = var3.next();
                    result.add(ConverterUtils.convert(tClass, obj));
                }
            }
            BeanUtils.copyProperties(dList, result);
            return result;
        }
        return null;
    }

    /**
     * 分页数据转换
     *
     * @param tClass   目标类型
     * @param pageInfo 源分页数据
     * @param <T>      目标类型
     * @return
     */
    public static <T> PageInfo<T> convert(Class<T> tClass, PageInfo pageInfo) {
        if (pageInfo == null)
            return new PageInfo<>();
        PageInfo<T> result = ConverterUtils.convert(PageInfo.class, pageInfo);
        List<T> list = ConverterUtils.convert(tClass, pageInfo.getList());
        result.setList(list);
        return result;
    }

}
