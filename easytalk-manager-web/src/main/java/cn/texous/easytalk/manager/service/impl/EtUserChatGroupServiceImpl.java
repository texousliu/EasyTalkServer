package cn.texous.easytalk.manager.service.impl;

import cn.texous.easytalk.manager.dao.EtUserChatGroupMapper;
import cn.texous.easytalk.manager.model.entity.EtUserChatGroup;
import cn.texous.easytalk.manager.service.api.EtUserChatGroupService;
import cn.texous.easytalk.manager.config.mysql.AbstractService;
import org.springframework.stereotype.Service;
// import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since  2019-09-02 10:55:33
 */
@Service
// @Transactional
public class EtUserChatGroupServiceImpl
        extends AbstractService<EtUserChatGroup> implements EtUserChatGroupService {

    @Resource
    private EtUserChatGroupMapper etUserChatGroupMapper;

}
