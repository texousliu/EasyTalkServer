package cn.texous.easytalk.manager.model.entity;

import java.util.Date;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * entity
 */
@Data
@Table(name = "et_user_group")
public class EtUserGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    /**
     * 用户code: et_user 表 code
     */
    @Column(name = "user_code")
    private String userCode;

    /**
     * 分组名称
     */
    private String name;

    /**
     * 创建时间
     */
    private Date created;

    /**
     * 创建人
     */
    @Column(name = "created_by")
    private String createdBy;

    /**
     * 更新时间
     */
    private Date updated;

    /**
     * 更新人
     */
    @Column(name = "updated_by")
    private String updatedBy;

}
