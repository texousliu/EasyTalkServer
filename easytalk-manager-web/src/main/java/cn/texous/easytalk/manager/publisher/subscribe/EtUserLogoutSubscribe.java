package cn.texous.easytalk.manager.publisher.subscribe;

import cn.texous.easytalk.commonutil.constant.RedisKey;
import cn.texous.easytalk.commonutil.util.RedisKeyUtil;
import cn.texous.easytalk.manager.model.dto.UserLoginDto;
import cn.texous.easytalk.manager.publisher.EtSubscribe;
import cn.texous.starter.redis.RedisClient;
import org.springframework.stereotype.Component;

/**
 * 用户登陆成功事件发布器
 *
 * @author Showa.L
 * @since 2019/7/25 19:21
 */
@Component
public class EtUserLogoutSubscribe implements EtSubscribe {

    /**
     * 订阅主题
     */
    public static final String TOPIC = "USER_LOGOUT";

    @Override
    public void onEvent(Object object) {
        UserLoginDto userLoginDto = (UserLoginDto) object;
        String key = RedisKeyUtil.getKey(RedisKey.USER_LOGIN_TOKEN, userLoginDto.getToken());
        if (userLoginDto.getToken() != null) {
            RedisClient.del(key);
        }
        // 在登陆的时候会做这一步
        //if (userLoginDto.getUser() != null) {
        //    String codeKey = RedisKeyUtil.getKey(
        //            RedisFormat.USER_LOGIN_CODE_TOKEN, userLoginDto.getUser().getCode());
        //    if (key.equals(RedisClient.get(codeKey)))
        //        RedisClient.del(codeKey);
        //}
        // TODO 需要发布 token 失效消息，用来失效聊天，让用户登出
    }

    @Override
    public String getTopic() {
        return TOPIC;
    }
}
