package cn.texous.easytalk.manager.service.api;

import cn.texous.easytalk.manager.config.mysql.Service;
import cn.texous.easytalk.manager.model.dto.MessageDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.entity.EtMessage;
import cn.texous.easytalk.manager.model.params.message.ListChatMessageParam;
import com.github.pagehelper.PageInfo;


/**
 * insert description here.
 *
 * @author Showa.L
 * @since 2019-09-02 10:55:33
 */
public interface EtMessageService extends Service<EtMessage> {

    PageInfo<MessageDto> listChatMessage(ListChatMessageParam param, UserDto userDto);

}
