package cn.texous.easytalk.manager.model.params.friend;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:57
 */
@Data
public class FindFriendByIdParam {

    @ApiModelProperty(
            required = true,
            notes = "查找的用户id",
            example = "123"
    )
    @NotNull
    private Long id;

}
