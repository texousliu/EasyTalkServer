package cn.texous.easytalk.manager.model.params.friend;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * insert description here
 *
 * @author Showa.L
 * @since 2019/9/4 17:57
 */
@Data
public class AgreeFriendAddParam {

    @ApiModelProperty(
            required = true,
            notes = "需要添加的好友的 code",
            example = "684833535206494212"
    )
    @NotNull
    private String code;

    @ApiModelProperty(
            required = true,
            notes = "将好友添加到哪个分组，分组 code",
            example = "684833535206494212"
    )
    @NotNull
    private String userGroupCode;

    @ApiModelProperty(
            notes = "备注名称",
            example = "基友0001"
    )
    private String remarkName;

}
