package cn.texous.easytalk.manager.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * dto
 */
@Data
public class MessageDto {

    private Long id;

    @ApiModelProperty(
            required = true,
            notes = "消息编码"
    )
    private String code;

    @ApiModelProperty(
            required = true,
            notes = "聊天 code：user_code 或 chat_group_code"
    )
    private String chatCode;

    @ApiModelProperty(
            required = true,
            notes = "消息发送人：et_user 表 code"
    )
    private String fromCode;

    @ApiModelProperty(
            required = true,
            notes = "消息内容"
    )
    private String content;

    @ApiModelProperty(
            required = true,
            notes = "聊天类型：0=单聊， 1=群聊"
    )
    private Integer type;

    @ApiModelProperty(
            required = true,
            notes = "状态：0=待发送，1=已发送，2=发送失败，已撤销，4=已删除"
    )
    private Integer status;

    @ApiModelProperty(
            required = true,
            notes = "消息发送时间"
    )
    private Date sendTime;

    @ApiModelProperty(
            required = true,
            notes = "消息创建时间"
    )
    private Date created;

}
