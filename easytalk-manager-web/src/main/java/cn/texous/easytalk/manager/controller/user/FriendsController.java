package cn.texous.easytalk.manager.controller.user;

import cn.texous.easytalk.commonutil.constant.Result;
import cn.texous.easytalk.commonutil.constant.ResultCode;
import cn.texous.easytalk.commonutil.constant.UserConst;
import cn.texous.easytalk.commonutil.util.CollectionUtils;
import cn.texous.easytalk.manager.controller.BaseController;
import cn.texous.easytalk.manager.model.dto.FindUserDto;
import cn.texous.easytalk.manager.model.dto.UserDto;
import cn.texous.easytalk.manager.model.params.friend.AddFriendParam;
import cn.texous.easytalk.manager.model.params.friend.AgreeFriendAddParam;
import cn.texous.easytalk.manager.model.params.friend.DeleteFriendParam;
import cn.texous.easytalk.manager.model.params.friend.FindFriendByIdParam;
import cn.texous.easytalk.manager.model.params.friend.FindFriendByNameParam;
import cn.texous.easytalk.manager.model.params.friend.FriendGroupChangeParam;
import cn.texous.easytalk.manager.model.params.friend.FriendRemarkChangeParam;
import cn.texous.easytalk.manager.service.api.EtUserFriendService;
import cn.texous.easytalk.manager.service.api.EtUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

/**
 * 好友操作 API
 *
 * @author Showa.L
 * @since 2019/9/2 11:26
 */
@Slf4j
@RestController
@RequestMapping("/friends")
@Api(value = "好友管理", tags = "好友管理接口")
public class FriendsController extends BaseController {

    @Autowired
    private EtUserService etUserService;
    @Autowired
    private EtUserFriendService etUserFriendService;

    @PostMapping("/add")
    @ApiOperation(value = "添加好友")
    public Result<Boolean> addFriend(@RequestBody @Valid AddFriendParam param) {
        UserDto userDto = currentUserNotNull();
        if (!etUserService.hasUser(param.getCode()))
            return Result.out(ResultCode.USER_NOT_EXISTS_ERROR);
        if (etUserFriendService.existsFriend(param.getCode(),
                userDto.getCode(), UserConst.Status.NORMAL.ordinal()))
            return Result.ok(true);
        boolean result = etUserService.addFriend(param, userDto);
        return Result.ok(result);
    }

    @PostMapping("/agree")
    @ApiOperation(value = "通过好友添加")
    public Result<Boolean> agreeFriend(@RequestBody @Valid AgreeFriendAddParam param) {
        UserDto userDto = currentUserNotNull();
        boolean result = etUserService.agreeFriendAdd(param, userDto);
        return Result.ok(result);
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除好友")
    public Result<Boolean> deleteFriends(@RequestBody @Valid DeleteFriendParam param) {
        if (CollectionUtils.isEmpty(param.getCodes()))
            return Result.ok(true);

        UserDto userDto = currentUserNotNull();
        boolean result = etUserService.deleteFriends(param, userDto);
        return Result.ok(result);
    }

    @PostMapping("/black")
    @ApiOperation(value = "拉黑好友")
    public Result<Boolean> blackFriend(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    @PostMapping("/shield")
    @ApiOperation(value = "屏蔽好友")
    public Result<Boolean> shieldFriend(@RequestBody Map<String, String> params) {
        return Result.ok();
    }

    @PostMapping("/find/id")
    @ApiOperation(value = "根据 id 查找用户")
    public Result<FindUserDto> findFriendById(@RequestBody @Valid FindFriendByIdParam param) {
        // 校验用户有没有登陆
        UserDto userDto = currentUserNotNull();
        FindUserDto result = etUserService.findUserById(param);
        return Result.ok(result);
    }

    @PostMapping("/find/name")
    @ApiOperation(value = "根据 username 查找用户")
    public Result<FindUserDto> findFriendByName(@RequestBody @Valid FindFriendByNameParam param) {
        // TODO 后期需要改成权限模式
        UserDto userDto = currentUserNotNull();
        FindUserDto result = etUserService.findUserByName(param);
        return Result.ok(result);
    }

    @PostMapping("/group/change")
    @ApiOperation(value = "修改好友所在分组")
    public Result<Boolean> changeFriendGroup(@RequestBody @Valid FriendGroupChangeParam param) {
        UserDto userDto = currentUserNotNull();
        boolean result = etUserService.changeFriendGroup(param, userDto);
        return Result.ok(result);
    }

    @PostMapping("/remark/change")
    @ApiOperation(value = "修改好友备注")
    public Result<Boolean> changeFriendRemark(@RequestBody @Valid FriendRemarkChangeParam param) {
        UserDto userDto = currentUserNotNull();
        boolean result = etUserService.changeFriendRemark(param, userDto);
        return Result.ok(result);
    }

}
