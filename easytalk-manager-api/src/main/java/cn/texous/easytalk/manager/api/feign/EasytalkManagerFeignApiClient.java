package cn.texous.easytalk.manager.api.feign;

import cn.texous.easytalk.commonutil.constant.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 对外 api
 *
 * @author leven
 * @since
 */
public interface EasytalkManagerFeignApiClient {

    /** 微服务名称 */
    String NAME = "EASYTALK-WEBSOCKET";

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    Result test();

}
