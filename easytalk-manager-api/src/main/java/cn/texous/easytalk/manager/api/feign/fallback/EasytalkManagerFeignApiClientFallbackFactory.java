package cn.texous.easytalk.manager.api.feign.fallback;

import cn.texous.easytalk.commonutil.constant.Result;
import cn.texous.easytalk.commonutil.util.GsonUtils;
import cn.texous.easytalk.manager.api.feign.EasytalkManagerFeignApiClient;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * fallback factory
 *
 * @author leven
 * @since
 */
@Slf4j
@Component
public class EasytalkManagerFeignApiClientFallbackFactory
        implements FallbackFactory<EasytalkManagerFeignApiClient> {

    @Override
    public EasytalkManagerFeignApiClient create(Throwable cause) {

        if (cause != null && cause.getMessage() != null && !"".equals(cause.getMessage())) {
            log.error("fallback reason was:" + cause.getMessage(), cause);
        }

        return new EasytalkManagerFeignApiClient() {

            @Override
            public Result test() {
                printError(null);
                return Result.fail("fail");
            }

            private void printError(Object params) {
                log.error(String.format("message=%s, 请求参数为=%s",
                        cause.getMessage(), GsonUtils.toJson(params)), cause);
            }

        };
    }

}
