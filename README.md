## 更新记录 ##
- 2019-10-10
    1. 添加 checkstyle 校验，获取具体的校验配置请点击 [这里][checkstyle]
    2. 修改 RedisClient 使用方式，使用 redis-starter-extend 方式，获取 redis-starter-extend 请点击 [这里][redis-starter-extend] 
    3. 调整 CodeGenerator 适应 checkstyle
    
- 2019-09-24
    1. 添加 H2 做数据库测试
    2. 添加 H2 初始方法
    3. 完成用户好友分组接口（不包含联动）
    
- 2019-09-23
    1. 完成用户管理
    2. 完成好友管理（拉黑，屏蔽后续补充）

- 2019-09-10
    1. 添加 swagger2 配置
    2. 添加测试 整合 mockit
    3. 登录和socket联动，新增添加好友操作
    4. 集成 swagger-bootstrap-ui
    5. 添加好友消息联动

- 2019-09-03
    1. 初始化基础 model，mapper，service
    2. 添加 redis 操作类
    3. 新增基础接口（只是接口定义，里面内容为 null）
    4. 完成用户注册、登陆、退出登陆（socket 模块没有做，需要做相应的操作）
    
- 2019-09-01
    1. 增加数据库初始化文件（初稿，待完善，无索引）
    2. 规范消息 type 为四位数，如： 0000， 1000.
    3. README 文件增加更新记录列表
    
- 2019-09-01 之前
    1. 划分项目模块
    2. 初始化项目
    3. 基础 websocket 服务编写
    4. 基础 web 服务搭建
    5. 。。。等等不记得了

## 项目说明 ##
### 目录结构 ###

- 项目分为四个模块
   1. easytalk-common-util: 存放公共工具类
   2. easytalk-manager-api: 微服务调用模块，是 easytalk-manager-web 提供给其他服务的调用接口
   3. easytalk-manager-web: 关系管理服务，主要提供 web 接口管理用户关系
   4. easttalk-websocket-service: websocket server 服务
   
### 运行依赖 ###
因为项目用了 注册中心和配置中心，所以启动服务之前需要启动 注册中心和配置中心，具体的 demo 请看：
[open_demo](https://gitee.com/texousliu/open-demo.git) 中的 demo-config-service 和 demo-eureka-service。
配置文件放在了 config 目录下

### 接口文档 ###
- 启动项目
- 浏览器输入：http://{host}:{port}/doc.html

### 项目简介 ###
- 本项目并不是用来研究 socket 或者 AIO/NIO 的，所以我们直接采用了 TIO, 如果需要研究 AIO/NIO 的同学可以看 TIO 源码或者 Netty 源码。 为什么不用 Netty,这个可以参考 TIO 笔者的对比说明，这边不再详细解释

- 本项目主要是业务层面的开发。用来熟悉业务开发，加强业务逻辑，简化业务代码。当然其中也涉及一些设计模式，算法，以及一些新技术的简单使用。再次说明，本项目并不是来研究源码的。

- 为了文档简洁性，特作如下定义
   1. 每个模块都使用最后的单词表示，比如：easytalk-common-util 则表示为 util

- 数据库初始化文件放在 web 下的 doc 文件夹中

- 数据库初始化文件由 navicate premium 12 导出，如遇语法错误，请自行检查 sql 语句

- 代码生成器 集成在 web 下的 test 中的 classpath 中，使用前请注意 CodeGenerator 顶部的注意事项


## 项目依赖说明
- [spring-boot-starter-extend starter扩展仓库][spring-boot-starter-extend]
- [commons 工具类仓库][commons]
- [archetype-stable Maven 骨架项目仓库][archetype-stable]
- [easy-talk 聊天仓库][easytalk]
- [open-demo 各种小例子仓库][open-demo]
- [web-parsing 网络爬虫仓库][web-parsing]
- [blog 博客文章仓库][blog]


[spring-boot-starter-extend]: https://gitee.com/texousliu/spring-boot-starter-extend
[commons]: https://gitee.com/texousliu/commons
[archetype-stable]: https://gitee.com/texousliu/archetype-stable
[easytalk]: https://gitee.com/texousliu/EasyTalkServer
[open-demo]: https://gitee.com/texousliu/open-demo
[web-parsing]: https://gitee.com/texousliu/WebParsingServer
[blog]: https://gitee.com/texousliu/blog

[checkstyle]: https://gitee.com/texousliu/blog/blob/master/checkstyle/checkstyle-825.xml
[redis-starter-extend]: https://gitee.com/texousliu/spring-boot-starter-extend